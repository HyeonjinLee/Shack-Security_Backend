package net.smartcoder.shack.dao;

import java.util.List;

import net.smartcoder.shack.model.User;

public interface UserDao {
	
	User findById(long id);
	
	User findByUsername(String username);
	
	List<User> findAllUser();

}
