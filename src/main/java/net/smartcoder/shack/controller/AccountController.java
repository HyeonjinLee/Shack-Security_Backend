package net.smartcoder.shack.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.smartcoder.shack.dao.UserDao;
import net.smartcoder.shack.model.User;

@RestController
public class AccountController {

	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public @ResponseBody List<User> usersList() {
        logger.debug("get users list");
        return userDao.findAllUser();
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable Long userId) {
        logger.debug("get user by id");
        return userDao.findById(userId);
    }
    
    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable String username) {
        logger.debug("get user by name");
        return userDao.findByUsername(username);
    }

//    @RequestMapping(value = "/users", method = RequestMethod.POST)
//    public @ResponseBody User saveUser(@RequestBody User user) {
//        logger.debug("save user");
//        userDao.save(user);
//        return user;
//    }
    
}
