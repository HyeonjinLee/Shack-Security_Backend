package net.smartcoder.shack;

public class ServerConstants {

    public final static String X_AUTH_TOKEN = "X-Auth-Token";
    public final static String XSRF_TOKEN = "XSRF-TOKEN";
    public final static String X_XSRF_TOKEN = "X-XSRF-TOKEN";

}
